# JW.org tools

## Functies
- Bij gedubde video's de Engelse track gesynchroniseerd afspelen
- In- en uitschakelen van een transparante achtergrond bij ondertiteling

## Installeren
1. Installeer de Tampermonkey-plugin in je browser: [https://www.tampermonkey.net/](https://www.tampermonkey.net/)
1. Installeer het script via deze link: https://regapictures-userscripts.gitlab.io/jworg-tools/bundle.user.js
1. Tampermonkey zal je vragen of je het script wil installeren, bevestig dat.
4. Als je JW.org al had openstaan, herlaad dan die pagina('s).

## Andere nuttige links

### Downloaden
(**Niet** voor de doorsnee gebruiker)  
https://gitlab.com/regapictures-userscripts/jworg-tools/-/jobs/artifacts/master/raw/dist/bundle.user.js?job=build

### Beta downloaden
https://gitlab.com/regapictures-userscripts/jworg-tools/-/jobs/artifacts/dev/raw/dist/bundle.user.js?job=build

## Ontwikkeling
**Belangrijke opmerking:** het kan nodig zijn om ofwel...
- ...een oudere versie van node te gebruiken (bv v14)
- ...bij compilatie/`serve` de optie `NODE_OPTIONS=--openssl-legacy-provider` te gebruiken