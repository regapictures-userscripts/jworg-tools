import { Feature } from "./feature/feature";
import { SubtitlesTransparentBackground } from "./feature/subtitles-transparant-background";
import { VideoEnglishAudioFeature } from "./feature/video-english-audio";
import { HtmlElements } from "./html-elements";
import { videoButtonsContainerId } from "./style/style";
import { VideoButtonsContainerFactory } from "./video-buttons-container-factory";
import { VideoDetector } from './video-detector';

const htmlElements = new HtmlElements();
const videoDetector = new VideoDetector(htmlElements);
const videoButtonsContainerFactory = new VideoButtonsContainerFactory(htmlElements, videoDetector);
const features: Feature[] = [
    new VideoEnglishAudioFeature(htmlElements, videoButtonsContainerFactory),
    new SubtitlesTransparentBackground(videoButtonsContainerFactory)
];

function run() {
    addGlobalStyles();
    runFeatures();
}

function addGlobalStyles() {
    GM_addStyle(`#${videoButtonsContainerId} { margin-top: 10px; display: flex; align-items: center; gap: 2px 4px; flex-wrap: wrap; } `);
}

function runFeatures() {
    features.forEach(feature => attemptInit(feature));
}

function attemptInit(feature: Feature) {
    try {
        feature.initialize();
    } catch(err) {
        console.error(err);
    }
}

jQuery(() => run());