export class HtmlElements {
    public videoElement(): JQuery<HTMLVideoElement> {
        return $('#videoPlayerInstance video');
    }

    public mediaItemLanguagePickerContainer(): JQuery<HTMLElement> {
        return $('div.jsItemLanguagePicker.mediaItemLanguage');
    }

    public getEnableSubtitlesListItem(): JQuery<HTMLElement> {
        // At the moment, there is is no stable way to detect the subtitles menu and buttons :(
        return $('#videoPlayerInstance div.vjs-menu:nth-child(10) > ul:nth-child(2) > li:nth-child(1)');
    }
}