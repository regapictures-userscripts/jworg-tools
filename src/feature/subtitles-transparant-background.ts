import { pairwise, startWith } from 'rxjs/operators';
import { globalStyleName } from '../style/style';
import { VideoButtonsContainerFactory } from '../video-buttons-container-factory';
import { Feature } from './feature';

export class SubtitlesTransparentBackground implements Feature {
    private static readonly transparentSubtitleBackgroundSettingKey = 'transparentSubtitleBackground';
    private static readonly buttonElementId = "jwOrgToolsToggleTransparentSubtitlesBackground";
    private static readonly cssSelector = '#videoPlayerInstance>.video-js>.vjs-text-track-display>div:first-child>div:first-child>div:first-child';
    private static readonly cssRule = 'background-color:rgba(0,0,0,0.5) !important';

    private cssElement: HTMLStyleElement | undefined;

    constructor(private videoButtonsContainerFactory: VideoButtonsContainerFactory) { }

    public get enabled(): boolean {
        return this.cssElement != undefined;
    }

    public initialize() {
        this.videoButtonsContainerFactory.created$
            .pipe(
                startWith(undefined),
                pairwise()
            )
            .subscribe(([previous, current]) => {
                if (previous != undefined) this.destroy();
                this.setup(current!.videoButtonsContainerElement);
            });
    }

    private setup(container: JQuery<HTMLElement>): void {
        const shouldBeEnabled: boolean = GM_getValue(SubtitlesTransparentBackground.transparentSubtitleBackgroundSettingKey) ?? false;
        if (shouldBeEnabled && !this.enabled) {
            this.toggleSubtitleTransparency();
        }
        this.displayToggleButton(container);
    }

    private destroy() {
        if (this.cssElement != undefined) {
            this.toggleSubtitleTransparency();
        }
        this.removeToggleButton();
    }

    private displayToggleButton(container: JQuery<HTMLElement>) {
        const content = this.determineToggleButtonContent(this.enabled);
        const button = $(`<button id="${SubtitlesTransparentBackground.buttonElementId}" class="${globalStyleName}">${content}</button>`)
            .on('click', () => {
                this.toggleSubtitleTransparency();
                button.html(this.determineToggleButtonContent(this.enabled));
            });
        container.append(button);
    }

    private toggleSubtitleTransparency() {
        if (this.cssElement == undefined) {
            this.cssElement = GM_addStyle(this.createStyle());
            GM_setValue(SubtitlesTransparentBackground.transparentSubtitleBackgroundSettingKey, true);
        } else {
            this.cssElement.remove();
            this.cssElement = undefined;
            GM_setValue(SubtitlesTransparentBackground.transparentSubtitleBackgroundSettingKey, false);
        }
    }

    private determineToggleButtonContent(isChecked: boolean) {
        return '<span class="secondaryButton" style="margin: 0px">'
            + `<span class="buttonIcon" style="font-size: 1.2em; transform: translateY(-0.06em);">${this.determineCheckboxCharacter(isChecked)}</span>`
            + '<span class="buttonIcon" style="font-family: media-player">&#xe60f;</span>'
            + '<span class="buttonText" style="margin-left: 0px">Transparantie</span>'
            + '</span>';
    }

    private determineCheckboxCharacter(isChecked: boolean) {
        return isChecked ? '&#x2611;' : '&#x2610;'
    }

    private createStyle() {
        return SubtitlesTransparentBackground.cssSelector + " { "
            + SubtitlesTransparentBackground.cssRule + " }";
    }

    private removeToggleButton() {
        $('#' + SubtitlesTransparentBackground.buttonElementId).remove();
    }
}