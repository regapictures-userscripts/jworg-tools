import { pairwise, startWith } from 'rxjs/operators';
import { HtmlElements } from '../html-elements';
import { globalStyleName } from '../style/style';
import { VideoButtonsContainerFactory } from '../video-buttons-container-factory';
import { Feature } from './feature';

export class VideoEnglishAudioFeature implements Feature {
    private readonly EventNamespace = ".video-english-audio";
    private readonly audioElementId = "jwOrgToolsSecondaryEnglishAudio";
    private readonly buttonElementId = "jwOrgToolsEnableEnglishAudioButton";
    private mainVideo: JQuery<HTMLVideoElement>;
    private audio: JQuery<HTMLAudioElement>;

    constructor(private htmlElements: HtmlElements, private videoButtonsContainerFactory: VideoButtonsContainerFactory) { }

    public initialize() {
        this.videoButtonsContainerFactory.created$
            .pipe(
                startWith(undefined),
                pairwise()
            )
            .subscribe(([previous, current]) => {
                if (previous != undefined) this.destroy();
                this.setup(current!.videoButtonsContainerElement, current!.videoElement);
            });
    }

    private setup(container: JQuery<HTMLElement>, mainVideo: JQuery<HTMLVideoElement>) {
        this.mainVideo = mainVideo;
        this.displayEnableButton(container, () => this.enableSubtitledEnglishAudio());
    }

    private destroy() {
        if (this.audio != undefined && this.audio.length > 0) {
            this.audio[0].pause();
        }
        this.removeAudioElement();
        this.removeEnableButton();
        this.teardownSynchronisation();
        this.setMainVideoMuted(false);
        // We're no longer disabling subtitles, because JW.org now remembers whether you've enabled them last time or not
        // We don't want to override that behavior
    }

    private enableSubtitledEnglishAudio() {
        this.determineAudioUrl()
                .then(audioUrl => {
                    if (audioUrl == undefined) {
                        console.warn('No audio URL detected. Aborting.');
                        return;
                    }

                    const wasPlaying = !this.mainVideo[0].paused;
                    if (wasPlaying) this.mainVideo[0].pause();
                    this.audio = <JQuery<HTMLAudioElement>>$('<audio id="' + this.audioElementId + '"></audio>')
                        .attr('src', audioUrl);
                    this.setupSynchronisation();
                    $('body').append(this.audio);
                    this.setMainVideoMuted(true);
                    this.enableSubtitles();
                    this.syncAudio();
                    if (wasPlaying) {
                        this.audio.one('canplay' + this.EventNamespace, () => {
                            this.setAudioMuted(true);
                            this.audio[0].play()
                                .then(() => {
                                    this.audio[0].pause();
                                    this.syncAudio();
                                    this.setAudioMuted(false);
                                    this.mainVideo[0].play();
                                })
                                .catch(reason => {
                                    console.error('Could not start audio first, falling back to setTimeout()', reason);
                                    this.setAudioMuted(false);
                                    setTimeout(() => this.mainVideo[0].play(), 1000);
                                });
                        });
                    }
                })
                .catch(err => console.error('Could not determine audio URL, reason:', err));
    }

    private displayEnableButton(container: JQuery<HTMLElement>, clickHandler: () => void) {
        const content = '<span class="buttonIcon" style="font-family:media-player">&#xe602;</span>'
            + '<span class="buttonText" style="margin-left: 0px">Engels</span>'
            + '<span class="buttonText" style="margin: 0px; font-size: 1.2em; position: relative; top: -0.05em">+</span>'
            + '<span class="buttonIcon" style="font-family:media-player">&#xe60f;</span>'
            + '<span class="buttonText" style="margin-left: 0px">Nederlands</span>';
        const wrappedContent = `<span class="secondaryButton" style="margin: 0px">${content}</span>`;
        const button = $(`<button id="${this.buttonElementId}" class="${globalStyleName}">${wrappedContent}</button>`)
            .on('click', () => {
                button.prop('disabled', true);
                button.html('<span style="font-size: 0.9em">&check; Ingeschakeld. Herlaad de<br>pagina om uit te schakelen.</span>');
                clickHandler();
            });
        container.append(button);
    }

    private removeAudioElement() {
        $('#' + this.audioElementId).remove();
    }

    private removeEnableButton() {
        $('#' + this.buttonElementId).remove();
    }

    private determineAudioUrl(): Promise<string> {
        const originalVideoUrl = this.mainVideo.attr('src');
        if (originalVideoUrl == undefined) {
            return Promise.reject('No original video URL');
        }

        const docId = this.determineDocumentId();
        if (docId.trim() === '') {
            return Promise.reject('No document id detected in location hash');
        }

        return this.requestEnglishVideoURL(docId);
    }

    private determineDocumentId(): string {
        const pathParts = window.location.hash.split('/');
        const lastIndex = pathParts.length - 1;
        return pathParts[lastIndex];
    }

    private requestEnglishVideoURL(docId: string): Promise<string> {
        return new Promise((resolve, reject) =>
            this.requestEnglishVideoInfo(docId, data => {
                const mediaFile = data == undefined || data.length === 0
                    ? undefined
                    : data[0].files?.find(file => file.label.startsWith('720'));
                const url = mediaFile?.progressiveDownloadURL;
                if (url != undefined) {
                    resolve(url);
                } else {
                    const errorMsg = 'Failed to find english video URL';
                    console.error(errorMsg, data);
                    reject(errorMsg);
                }
            })
        );
    }

    private requestEnglishVideoInfo(docId: string, successFn: (data: MediaInfo[]) => void) {
        $.get(`https://b.jw-cdn.org/apis/mediator/v1/media-items/E/${docId}?clientType=www`, data => successFn(data.media));
    }

    private setupSynchronisation() {
        this.mainVideo.on('pause' + this.EventNamespace, () => this.pauseAndSyncAudio());
        this.mainVideo.on('seeking' + this.EventNamespace, () => this.pauseAndSyncAudio());
        this.mainVideo.on('stalled' + this.EventNamespace, () => this.pauseAndSyncAudio());
        this.mainVideo.on('waiting' + this.EventNamespace, () => this.pauseAndSyncAudio());
        this.mainVideo.on('play' + this.EventNamespace, () => this.audio[0].play());
        this.mainVideo.on('playing' + this.EventNamespace, () => this.audio[0].play());
        this.mainVideo.on('seeked' + this.EventNamespace, () => {
            if (!this.mainVideo[0].paused) this.audio[0].play();
            this.syncAudio();
        });
    }

    private teardownSynchronisation() {
        if (this.mainVideo == undefined) return;
        this.mainVideo.off('pause' + this.EventNamespace);
        this.mainVideo.off('seeking' + this.EventNamespace);
        this.mainVideo.off('stalled' + this.EventNamespace);
        this.mainVideo.off('waiting' + this.EventNamespace);
        this.mainVideo.off('play' + this.EventNamespace);
        this.mainVideo.off('playing' + this.EventNamespace);
        this.mainVideo.off('seeked' + this.EventNamespace);
    }

    private pauseAndSyncAudio(): void {
        this.audio[0].pause();
        this.syncAudio();
    }

    private syncAudio(): void {
        this.audio[0].currentTime = this.mainVideo[0].currentTime;
    }

    private setMainVideoMuted(muted: boolean) {
        if (this.mainVideo != undefined && this.mainVideo.length > 0) {
            this.mainVideo[0].muted = muted;
        }
    }

    private setAudioMuted(muted: boolean) {
        if (this.audio != undefined && this.audio.length > 0) {
            this.audio[0].muted = muted;
        }
    }

    private enableSubtitles(): void {
        this.htmlElements.getEnableSubtitlesListItem().trigger('click');
    }
}

interface MediaInfo {
    files: MediaFile[];
}

interface MediaFile {
    label: string;
    progressiveDownloadURL: string;
}