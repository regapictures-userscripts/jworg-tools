export interface Feature {
    initialize(): void;
}