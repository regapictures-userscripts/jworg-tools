import { fromEvent, interval, merge, Observable, of } from "rxjs";
import { debounceTime, filter, map, shareReplay, switchMap, take } from "rxjs/operators";
import { HtmlElements } from "./html-elements";

export class VideoDetector {
    private onDetect$: Observable<JQuery<HTMLVideoElement>>;

    constructor(private htmlElements: HtmlElements, private retryIntervalMillis: number = 1000, private maxTries: number = 8) {
        const init$ = merge(
            of(undefined),
            fromEvent(window, 'locationchange').pipe(map(() => undefined)),
            fromEvent(window, 'hashchange').pipe(map(() => undefined))
        );
        this.onDetect$ = init$.pipe(
            debounceTime(100),
            switchMap(() => interval(this.retryIntervalMillis).pipe(
                take(this.maxTries),
                map(i => ({ attemptIndex: i, element: this.htmlElements.videoElement() })),
                filter(d => d.element.length > 0 && d.element.attr('src') != undefined),
                map(d => d.element),
                take(1)
            )),
            shareReplay(1)
        );
    }

    public get detection() {
        return this.onDetect$;
    }
}