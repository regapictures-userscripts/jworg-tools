import { Observable } from "rxjs";
import { filter, map, shareReplay } from "rxjs/operators";
import { HtmlElements } from "./html-elements";
import { videoButtonsContainerId } from "./style/style";
import { VideoDetector } from "./video-detector";

export class VideoButtonsContainerFactory {
    
    public readonly created$: Observable<VideoButtonsContainerCreationEvent>;

    constructor(private htmlElements: HtmlElements, videoDetector: VideoDetector) {
        this.created$ = videoDetector.detection.pipe(
            filter(_ => this.findVideoButtonsContainer().length === 0),
            map(videoElement => ({
                videoElement,
                videoButtonsContainerElement: this.createVideoButtonsContainer()
            })),
            shareReplay(1)
        );
    }

    private findVideoButtonsContainer(): JQuery<HTMLElement> {
        return this.htmlElements.mediaItemLanguagePickerContainer().find(`#${videoButtonsContainerId}`);
    }

    private createVideoButtonsContainer(): JQuery<HTMLElement> {
        const existingContainer = this.findVideoButtonsContainer();
        if (existingContainer.length === 0) {
            const container = $(`<div id="${videoButtonsContainerId}"></div>`);
            this.htmlElements.mediaItemLanguagePickerContainer().append(container);
            return container;
        } else {
            return existingContainer;
        }
    }
}

export interface VideoButtonsContainerCreationEvent {
    readonly videoElement: JQuery<HTMLVideoElement>;
    readonly videoButtonsContainerElement: JQuery<HTMLElement>;
}