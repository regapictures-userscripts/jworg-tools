const scriptName = 'JW.org tools';
const tamperMonkeyHeaders = {
    name: scriptName,
    namespace: 'https://regapictures-userscripts.gitlab.io/',
    source: 'https://gitlab.com/regapictures-userscripts/jworg-tools',
    version: `[version]`,
    description: 'Extensions for JW.org',
    author: 'regapictures',
    downloadURL: 'https://regapictures-userscripts.gitlab.io/jworg-tools/bundle.user.js',
    updateURL: 'https://regapictures-userscripts.gitlab.io/jworg-tools/bundle.meta.js',
    include: 'https://www.jw.org/*',
    grant: ['GM_addStyle', 'GM_setValue', 'GM_getValue']
}
const betaHeaderOverrides = {
    name: `${scriptName} (beta)`,
    version: `[version]-beta.${Math.floor((new Date()).getTime() / 1000)}`,
    downloadURL: 'https://gitlab.com/regapictures-userscripts/jworg-tools/-/jobs/artifacts/dev/raw/dist/bundle.user.js?job=build',
    updateURL: 'https://gitlab.com/regapictures-userscripts/jworg-tools/-/jobs/artifacts/dev/raw/dist/bundle.meta.js?job=build',
};
const developmentHeaderOverrides = {
    name: 'Local Development',
    namespace: 'http://localhost:8080/',
    version: `[version]-build.${Math.floor((new Date()).getTime() / 1000)}.[buildNo]`,
    description: 'Development plugin. The contents of this plugin will change as you switch between projects.',
    author: 'local-dev',
    downloadURL: 'http://localhost:8080/bundle.user.js',
    updateURL: 'http://localhost:8080/bundle.meta.js'
}

const path = require('path');
const merge = require('webpack-merge');
const WebpackUserscript = require('webpack-userscript');
const dev = process.env.NODE_ENV === 'development';
const isBeta = process.env.CI_COMMIT_BRANCH === 'dev';

const finalTamperMonkeyHeaders =
    dev ? merge(tamperMonkeyHeaders, developmentHeaderOverrides) :
    isBeta ? merge(tamperMonkeyHeaders, betaHeaderOverrides) :
    tamperMonkeyHeaders;

module.exports = {
    mode: dev ? 'development' : 'production',
    entry: {
        'bundle': path.resolve(__dirname, 'src', 'index.ts')
    },
    module: {
        rules: [{
            test: /\.tsx?$/,
            use: 'ts-loader',
            exclude: /node_modules/
        }]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    optimization: {
        minimize: false
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist')
    },
    plugins: [
        new WebpackUserscript({ headers: finalTamperMonkeyHeaders })
    ]
};